﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UriComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = FullMatch();
            Console.WriteLine("Comparison status is : {0}", result);

            result = ImplicitMatch();
            Console.WriteLine("Comparison status is : {0}", result);

            result = OneSegmentMissing();
            Console.WriteLine("Comparison status is : {0}", result);

            result = IncorrectScheme();
            Console.WriteLine("Comparison status is : {0}", result);

            result = SegmentsMissing();
            Console.WriteLine("Comparison status is : {0}", result);

            result = CharacterMissing();
            Console.WriteLine("Comparison status is : {0}", result);

            result = CharacterAdditional();
            Console.WriteLine("Comparison status is : {0}", result);

            result = IncorrectPort();
            Console.WriteLine("Comparison status is : {0}", result);

            result = IncorrectHost();
            Console.WriteLine("Comparison status is : {0}", result);

            result = IncorrectDomainPrefix();
            Console.WriteLine("Comparison status is : {0}", result);

            result = SameCaseBasedComparison();
            Console.WriteLine("Comparison status is : {0}", result);

            result = CaseMismatchComparison();
            Console.WriteLine("Comparison status is : {0}", result);

            Console.ReadKey();
        }

        private static int FullMatch()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost:443/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int ImplicitMatch()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int OneSegmentMissing()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/account/";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int IncorrectScheme()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "http://localhost/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int SegmentsMissing()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int CharacterMissing()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/account/authoriz";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int CharacterAdditional()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/account/authorized";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int IncorrectPort()
        {
            string uriOne = "https://localhost:4430/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhost/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int IncorrectHost()
        {
            string uriOne = "https://localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://localhosts/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int IncorrectDomainPrefix()
        {
            string uriOne = "https://www.localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://www2.localhost/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int SameCaseBasedComparison()
        {
            string uriOne = "https://www.localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://www.localhost/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }
        private static int CaseMismatchComparison()
        {
            string uriOne = "https://www.localhost:443/account/authorize?key=value1%20f%value2";
            string uriTwo = "https://www.LOCALHOST/account/authorize";

            int result = CompareUris(uriOne, uriTwo);
            return result;
        }

        private static int CompareUris(string uriOne, string uriTwo)
        {
            return Uri.Compare(new Uri(uriOne), new Uri(uriTwo), UriComponents.HostAndPort | UriComponents.Path | UriComponents.SchemeAndServer | UriComponents.NormalizedHost, UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase);
        }
    }
}
